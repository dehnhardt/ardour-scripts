# LUA-Scripts for the Digital Audio Workstations Ardour and Harrison Mixbus

I usually use the same track colours for the same instruments and often have a similar name for the tracks and buses.
However, I find colouring the tracks quite time-consuming and therefore I have extended the LUA script Trackorganizer a little.
There are three scripts:
**Settings for Mixer Strip Configuration** (strip-config-settings.lua), **Mixer Strip Configuration** (strip-config.lua) and **Coloring Strip When Added** (strip-add-coloring-hook.lua).

With the first script, you can define Named Colours, as well as specify tags with which the names of the tracks and buses are compared and which then assign the corresponding colour.
With the second script, the assignment can be carried out - either automatically or manually - and names and comments can be assigned.
With the third script, the cooring rules are applied to newly creates strips

* Settings for Mixer Strip Configuration   
  Here you can define the names, colrs and matching tags. If no tags are define, no automatic coloring will be applied

* Mixer Strip Configuration   
  Before the Dialog with the settings appears, you first will be asked if the colors should be assigned automatically
  After that, all tracks are diosplayed with the already added colrs or the newly matched colors. You can always stop the changes when cancelling the Dialog

* Coloring Strip When Added   
  When a new strip is added, the color rules are applied immediatly. So there might be no need to call the *Mixer Strip Configuration*.
  
Details see: https://codeberg.org/dehnhardt/ardour-scripts/wiki/Home

## Changes

2022-05-09 Fix error when a route has no name